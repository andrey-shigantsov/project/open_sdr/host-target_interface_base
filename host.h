/*
 * host.h
 *
 *  Created on: 2 февр. 2018 г.
 *      Author: svetozar
 */

#ifndef SDR_HOST_TARGET_INTERFACE_BASE_HOST_H_
#define SDR_HOST_TARGET_INTERFACE_BASE_HOST_H_

#ifdef __cplusplus
extern "C"{
#endif

typedef UInt8_t HOST_DataId_t;
#define HOST_SetParamsId 0x00
#define HOST_CommandId 0x01

#define HOST_UserData_StartId 0x10

typedef enum
{
  HOST_getAll = 0x00,
  HOST_getFormat,
  HOST_getState,

  HOST_cmdSTART = 0x10,
  HOST_cmdSTEP,
  HOST_cmdPAUSE,
  HOST_cmdCONTINUE,
  HOST_cmdRESET,
  HOST_cmdSTOP,

  HOST_setValue
} HOST_Command_t;

#ifdef __cplusplus
}
#endif

#endif /* SDR_HOST_TARGET_INTERFACE_BASE_HOST_H_ */
