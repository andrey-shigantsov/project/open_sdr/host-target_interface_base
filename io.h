/*
 * io.h
 *
 *  Created on: 3 авг. 2018 г.
 *      Author: svetozar
 */

#ifndef SDR_HOST_INTERFACE_BASE_IO_H_
#define SDR_HOST_INTERFACE_BASE_IO_H_

#include <SDR/BASE/common.h>
#include <SDR/BASE/Multiplex/symMultiplex.h>

#ifdef __cplusplus
extern "C" {
#endif


#define SDR_IO_MUX_HEADER_SIZE (2+2) // size + crc

Bool_t IO_Mux_prepare_for_write(symMultiplex_t * This);
Bool_t IO_Mux_prepare_for_send(symMultiplex_t * This);

Bool_t IO_Mux_prepare_for_read(symMultiplex_t * This, UInt16_t * pduSize, Bool_t * crcOk);

#ifdef __cplusplus
}
#endif

#endif /* SDR_HOST_INTERFACE_BASE_IO_H_ */
